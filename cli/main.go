package main

import (
	"flag"
	ds "gitlab.com/andreynech/dsched/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"time"
)

func main() {
	// Parse command line parameters
	endpoint := flag.String("e", "localhost:50051", "Host:port to connect")
	list := flag.Bool("l", false, "List scheduled tasks")
	schedule := flag.Bool("s", false, "Schedule task. -c and -a arguments are required in this case")
	cron := flag.String("c", "", "Statement in crontab format describes when to execute the command")
	action := flag.String("a", "", "The command to execute at time specified by -c parameter")
	remove := flag.Int("r", 0, "Remove the task with specified id from schedule")
	purge := flag.Bool("p", false, "Purge all scheduled tasks")
	flag.Parse()

	// Set up a connection to the server.
	conn, err := grpc.Dial(*endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	} else {
		log.Printf("Connected to %s", *endpoint)
	}
	defer conn.Close()
	scheduler := ds.NewSchedulerClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	empty := &ds.Empty{}

	if *list {
		taskList, err := scheduler.List(ctx, empty)
		if err != nil {
			log.Fatalf("Error: %v", err)
		} else {
			log.Printf("Scheduled entities:")
			for _, t := range taskList.GetTasks() {
				log.Printf("%d %s %s", t.GetId(), t.GetCron(), t.GetAction())
			}
		}
	}

	if *schedule {
		if cron == nil {
			log.Fatal("-c option is required to schedule task")
		} else {
			if action == nil {
				log.Fatal("-a option is required to schedule task")
			} else {
				// Task ID is ignored, hence id=0 here.
				// Returned task has properly initialized ID
				task := &ds.Task{Id: 0, Cron: *cron, Action: *action}
				entity, err := scheduler.Add(ctx, task)
				if err != nil {
					log.Fatalf("Error: %v", err)
				} else {
					log.Printf("Task scheduled with ID: %d", entity.GetId())
				}
			}
		}
	}

	if *remove != 0 {
		// Only task ID matters here. All other attributes are ignored
		task := &ds.Task{Id: uint32(*remove), Cron: "", Action: ""}
		_, err := scheduler.Remove(ctx, task)
		if err != nil {
			log.Fatalf("Error: %v", err)
		} else {
			log.Println("Task removed")
		}
	}

	if *purge {
		_, err := scheduler.RemoveAll(ctx, empty)
		if err != nil {
			log.Fatalf("Error: %v", err)
		} else {
			log.Println("All tasks removed")
		}
	}
}
