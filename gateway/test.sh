#!/bin/sh

curl 'http://localhost:8080/v1/scheduler/list'
curl -d '{"id":0, "cron":"@every 0h00m10s", "action":"ls"}' -X POST 'http://localhost:8080/v1/scheduler/add'
curl -d '{"id":0, "cron":"0 30 * * * *", "action":"ls -l"}' -X POST 'http://localhost:8080/v1/scheduler/add'
curl -d '{"id":0, "cron":"@every 0h00m05s", "action":"ls -la"}' -X POST 'http://localhost:8080/v1/scheduler/add'
curl -d '{"id":2}' -X POST 'http://localhost:8080/v1/scheduler/remove'
curl 'http://localhost:8080/v1/scheduler/list'
curl -X POST 'http://localhost:8080/v1/scheduler/removeall'
curl 'http://localhost:8080/v1/scheduler/list'
curl -d '{"id":0, "cron":"@every 0h00m10s", "action":"ls -l"}' -X POST 'http://localhost:8080/v1/scheduler/add'
curl 'http://localhost:8080/v1/scheduler/list'
