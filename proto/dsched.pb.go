// Code generated by protoc-gen-go. DO NOT EDIT.
// source: dsched.proto

package dscheduler

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "google.golang.org/genproto/googleapis/api/annotations"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Task struct {
	Id                   uint32   `protobuf:"varint,10,opt,name=id,proto3" json:"id,omitempty"`
	Cron                 string   `protobuf:"bytes,20,opt,name=cron,proto3" json:"cron,omitempty"`
	Action               string   `protobuf:"bytes,30,opt,name=action,proto3" json:"action,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Task) Reset()         { *m = Task{} }
func (m *Task) String() string { return proto.CompactTextString(m) }
func (*Task) ProtoMessage()    {}
func (*Task) Descriptor() ([]byte, []int) {
	return fileDescriptor_dsched_a94e018e6bc245ce, []int{0}
}
func (m *Task) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Task.Unmarshal(m, b)
}
func (m *Task) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Task.Marshal(b, m, deterministic)
}
func (dst *Task) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Task.Merge(dst, src)
}
func (m *Task) XXX_Size() int {
	return xxx_messageInfo_Task.Size(m)
}
func (m *Task) XXX_DiscardUnknown() {
	xxx_messageInfo_Task.DiscardUnknown(m)
}

var xxx_messageInfo_Task proto.InternalMessageInfo

func (m *Task) GetId() uint32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Task) GetCron() string {
	if m != nil {
		return m.Cron
	}
	return ""
}

func (m *Task) GetAction() string {
	if m != nil {
		return m.Action
	}
	return ""
}

type TaskList struct {
	Tasks                []*Task  `protobuf:"bytes,10,rep,name=tasks,proto3" json:"tasks,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TaskList) Reset()         { *m = TaskList{} }
func (m *TaskList) String() string { return proto.CompactTextString(m) }
func (*TaskList) ProtoMessage()    {}
func (*TaskList) Descriptor() ([]byte, []int) {
	return fileDescriptor_dsched_a94e018e6bc245ce, []int{1}
}
func (m *TaskList) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TaskList.Unmarshal(m, b)
}
func (m *TaskList) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TaskList.Marshal(b, m, deterministic)
}
func (dst *TaskList) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TaskList.Merge(dst, src)
}
func (m *TaskList) XXX_Size() int {
	return xxx_messageInfo_TaskList.Size(m)
}
func (m *TaskList) XXX_DiscardUnknown() {
	xxx_messageInfo_TaskList.DiscardUnknown(m)
}

var xxx_messageInfo_TaskList proto.InternalMessageInfo

func (m *TaskList) GetTasks() []*Task {
	if m != nil {
		return m.Tasks
	}
	return nil
}

// Dummy message for the case where no parameters needed
type Empty struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Empty) Reset()         { *m = Empty{} }
func (m *Empty) String() string { return proto.CompactTextString(m) }
func (*Empty) ProtoMessage()    {}
func (*Empty) Descriptor() ([]byte, []int) {
	return fileDescriptor_dsched_a94e018e6bc245ce, []int{2}
}
func (m *Empty) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Empty.Unmarshal(m, b)
}
func (m *Empty) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Empty.Marshal(b, m, deterministic)
}
func (dst *Empty) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Empty.Merge(dst, src)
}
func (m *Empty) XXX_Size() int {
	return xxx_messageInfo_Empty.Size(m)
}
func (m *Empty) XXX_DiscardUnknown() {
	xxx_messageInfo_Empty.DiscardUnknown(m)
}

var xxx_messageInfo_Empty proto.InternalMessageInfo

func init() {
	proto.RegisterType((*Task)(nil), "dscheduler.Task")
	proto.RegisterType((*TaskList)(nil), "dscheduler.TaskList")
	proto.RegisterType((*Empty)(nil), "dscheduler.Empty")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// SchedulerClient is the client API for Scheduler service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type SchedulerClient interface {
	// List scheduled tasks
	List(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*TaskList, error)
	// Add new task to scheduler.
	// Returns same Task message with initialized id field
	Add(ctx context.Context, in *Task, opts ...grpc.CallOption) (*Task, error)
	// Remove task from scheduler. Only id field is significant
	// with this function. Other Task's fields are ignored.
	Remove(ctx context.Context, in *Task, opts ...grpc.CallOption) (*Empty, error)
	// Remove all scheduled tasks
	RemoveAll(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Empty, error)
}

type schedulerClient struct {
	cc *grpc.ClientConn
}

func NewSchedulerClient(cc *grpc.ClientConn) SchedulerClient {
	return &schedulerClient{cc}
}

func (c *schedulerClient) List(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*TaskList, error) {
	out := new(TaskList)
	err := c.cc.Invoke(ctx, "/dscheduler.Scheduler/List", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *schedulerClient) Add(ctx context.Context, in *Task, opts ...grpc.CallOption) (*Task, error) {
	out := new(Task)
	err := c.cc.Invoke(ctx, "/dscheduler.Scheduler/Add", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *schedulerClient) Remove(ctx context.Context, in *Task, opts ...grpc.CallOption) (*Empty, error) {
	out := new(Empty)
	err := c.cc.Invoke(ctx, "/dscheduler.Scheduler/Remove", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *schedulerClient) RemoveAll(ctx context.Context, in *Empty, opts ...grpc.CallOption) (*Empty, error) {
	out := new(Empty)
	err := c.cc.Invoke(ctx, "/dscheduler.Scheduler/RemoveAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SchedulerServer is the server API for Scheduler service.
type SchedulerServer interface {
	// List scheduled tasks
	List(context.Context, *Empty) (*TaskList, error)
	// Add new task to scheduler.
	// Returns same Task message with initialized id field
	Add(context.Context, *Task) (*Task, error)
	// Remove task from scheduler. Only id field is significant
	// with this function. Other Task's fields are ignored.
	Remove(context.Context, *Task) (*Empty, error)
	// Remove all scheduled tasks
	RemoveAll(context.Context, *Empty) (*Empty, error)
}

func RegisterSchedulerServer(s *grpc.Server, srv SchedulerServer) {
	s.RegisterService(&_Scheduler_serviceDesc, srv)
}

func _Scheduler_List_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SchedulerServer).List(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dscheduler.Scheduler/List",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SchedulerServer).List(ctx, req.(*Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Scheduler_Add_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Task)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SchedulerServer).Add(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dscheduler.Scheduler/Add",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SchedulerServer).Add(ctx, req.(*Task))
	}
	return interceptor(ctx, in, info, handler)
}

func _Scheduler_Remove_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Task)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SchedulerServer).Remove(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dscheduler.Scheduler/Remove",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SchedulerServer).Remove(ctx, req.(*Task))
	}
	return interceptor(ctx, in, info, handler)
}

func _Scheduler_RemoveAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SchedulerServer).RemoveAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dscheduler.Scheduler/RemoveAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SchedulerServer).RemoveAll(ctx, req.(*Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _Scheduler_serviceDesc = grpc.ServiceDesc{
	ServiceName: "dscheduler.Scheduler",
	HandlerType: (*SchedulerServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "List",
			Handler:    _Scheduler_List_Handler,
		},
		{
			MethodName: "Add",
			Handler:    _Scheduler_Add_Handler,
		},
		{
			MethodName: "Remove",
			Handler:    _Scheduler_Remove_Handler,
		},
		{
			MethodName: "RemoveAll",
			Handler:    _Scheduler_RemoveAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "dsched.proto",
}

func init() { proto.RegisterFile("dsched.proto", fileDescriptor_dsched_a94e018e6bc245ce) }

var fileDescriptor_dsched_a94e018e6bc245ce = []byte{
	// 324 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0xd1, 0x4d, 0x4e, 0x83, 0x40,
	0x14, 0x07, 0xf0, 0x30, 0xfd, 0xd0, 0xbe, 0xaa, 0xb1, 0x13, 0x52, 0x11, 0x1b, 0x6d, 0x66, 0x61,
	0x9a, 0x2e, 0x4a, 0xc4, 0x5d, 0x77, 0x25, 0x35, 0x2e, 0x34, 0xa6, 0xa1, 0x7a, 0x80, 0x91, 0x21,
	0x95, 0x74, 0xca, 0x10, 0x66, 0x6c, 0xe2, 0xd6, 0x2b, 0x78, 0x03, 0x4f, 0xe3, 0xde, 0x2b, 0x78,
	0x10, 0x33, 0x83, 0x5f, 0x15, 0x76, 0xbc, 0xf7, 0xfe, 0xef, 0x97, 0x07, 0xc0, 0x0e, 0x93, 0xd1,
	0x43, 0xcc, 0x46, 0x59, 0x2e, 0x94, 0xc0, 0x50, 0x54, 0x8f, 0x3c, 0xce, 0xdd, 0xde, 0x42, 0x88,
	0x05, 0x8f, 0x3d, 0x9a, 0x25, 0x1e, 0x4d, 0x53, 0xa1, 0xa8, 0x4a, 0x44, 0x2a, 0x8b, 0x24, 0x09,
	0xa0, 0x7e, 0x4b, 0xe5, 0x12, 0xef, 0x01, 0x4a, 0x98, 0x03, 0x7d, 0x6b, 0xb0, 0x1b, 0xa2, 0x84,
	0x61, 0x0c, 0xf5, 0x28, 0x17, 0xa9, 0x63, 0xf7, 0xad, 0x41, 0x2b, 0x34, 0xcf, 0xb8, 0x0b, 0x4d,
	0x1a, 0xe9, 0x65, 0xe7, 0xd8, 0x74, 0xbf, 0x2a, 0xe2, 0xc3, 0xb6, 0x36, 0xae, 0x13, 0xa9, 0xf0,
	0x29, 0x34, 0x14, 0x95, 0x4b, 0xe9, 0x40, 0xbf, 0x36, 0x68, 0xfb, 0xfb, 0xa3, 0xdf, 0x4b, 0x46,
	0x3a, 0x14, 0x16, 0x63, 0xb2, 0x05, 0x8d, 0x8b, 0x55, 0xa6, 0x9e, 0xfc, 0x37, 0x04, 0xad, 0xf9,
	0x77, 0x04, 0x5f, 0x41, 0xdd, 0x30, 0x9d, 0xbf, 0x7b, 0x26, 0xe8, 0xda, 0xff, 0x29, 0x1d, 0x24,
	0xee, 0xf3, 0xfb, 0xc7, 0x0b, 0xb2, 0x31, 0xf6, 0xd6, 0x67, 0xde, 0xcf, 0xdc, 0xe3, 0x1a, 0xb9,
	0x84, 0xda, 0x84, 0x31, 0x5c, 0xba, 0xc1, 0x2d, 0x75, 0x48, 0xcf, 0x30, 0x5d, 0xd2, 0xd9, 0x64,
	0x28, 0x63, 0x63, 0x6b, 0x88, 0x6f, 0xa0, 0x19, 0xc6, 0x2b, 0xb1, 0x8e, 0x2b, 0xac, 0xf2, 0xa5,
	0xe4, 0xc4, 0x60, 0x87, 0xc4, 0xde, 0xc4, 0x72, 0x43, 0x68, 0xef, 0x0e, 0x5a, 0x85, 0x37, 0xe1,
	0xbc, 0xea, 0x55, 0x2b, 0x4c, 0x62, 0xcc, 0x1e, 0x39, 0xa8, 0x32, 0x29, 0xe7, 0x63, 0x6b, 0x18,
	0x1c, 0x41, 0xc3, 0xfc, 0xd4, 0xa0, 0x3d, 0x35, 0x5f, 0x74, 0xa6, 0x8b, 0x99, 0xf5, 0x8a, 0xd0,
	0x74, 0x7e, 0xdf, 0x34, 0xa3, 0xf3, 0xcf, 0x00, 0x00, 0x00, 0xff, 0xff, 0x4f, 0xfd, 0xeb, 0x42,
	0x29, 0x02, 0x00, 0x00,
}
